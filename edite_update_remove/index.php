<?php
require 'db.php';
$sql = 'SELECT * FROM employé';
$statement = $connection->prepare($sql);
$statement->execute();
$employé = $statement->fetchAll(PDO::FETCH_OBJ);
?>
<?php require 'header.php'; ?>
<div class="container">
 <div class="card mt-5">
  <div class="card-header">
   <h2>Tous les employé</h2>
  </div>
  <div class="card-body">
   <table class="table table-bordered">
    <tr>
     <th>ID</th>
     <th>CIN</th>
     <th>Nom complet</th>
     <th>Date de naissance</th>
     <th>Date d'embauche</th>
     <th>Salaire</th>
     <th>Service</th>
     <th>Action</th>
    </tr>
    <?php foreach($employé as $person): ?>
     <tr>
      <td><?= $person->id; ?></td>
      <td><?= $person->cin; ?></td>
      <td><?= $person->nom; ?></td>
      <td><?= $person->dateN; ?></td>
      <td><?= $person->dateD; ?></td>
      <td><?= $person->salaire; ?></td>
      <td><?= $person->service; ?></td>
      <td>
       <a href="edit.php?id=<?= $person->id ?>" class="btn btn-info">Edit</a>
       <a onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet enregistrement?')" href="delete.php?id=<?= $person->id ?>" class='btn btn-danger'>Supprimer</a>
      </td>
     </tr>
    <?php endforeach; ?>
   </table>
  </div>
 </div>
</div>
<div class="container my-5">



