<?php require 'header.php';?>

<form method="get">
    <div class="container header_2 header_22 mt-5 mb-3">
    <div class="card mt-5">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <form action="" method="get">
                        <div class="input-group mb-1">
                            <input type="text" name="search" required  class="form-control" placeholder="Search data">
                            <button type="submit" name="btn-search" class="btn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</form>


<?php
require 'db.php';
if(isset($_GET['btn-search'])){
    $search=$_GET['search'];
    $sql = "SELECT * FROM employé where CONCAT(id,cin,nom,dateN,dateD,salaire,service) LIKE '%$search%' ";
    $statement = $connection->prepare($sql);
    $statement->execute();
    $employé = $statement->fetchAll(PDO::FETCH_OBJ);

?>
<?php  ?>
<div class="container">
 <div class="card mt-25">
  <div class="card-header">
   <h2>Tous les étudiants</h2>
  </div>
  <div class="card-body">
   <table class="table table-bordered">
   <tr>
     <th>ID</th>
     <th>CIN</th>
     <th>Nom complet</th>
     <th>Date de naissance</th>
     <th>Date d'embauche</th>
     <th>Salaire</th>
     <th>Service</th>
     <th>Action</th>
    </tr>
    
    <?php foreach($employé as $person): ?>
     <tr>
        <td><?= $person->id; ?></td>
        <td><?= $person->cin; ?></td>
        <td><?= $person->nom; ?></td>
        <td><?= $person->dateN; ?></td>
        <td><?= $person->dateD; ?></td>
        <td><?= $person->salaire; ?></td>
        <td><?= $person->service; ?></td>
      <td>
       <a href="edit.php?id=<?= $person->id ?>" class="btn btn-info">Edit</a>
       <a onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet enregistrement?')" href="delete.php?id=<?= $person->id ?>" class='btn btn-danger'>Supprimer</a>
      </td>
     </tr>
    <?php endforeach; ?>
    <?php } ?>
   </table>
  </div>
 </div>
</div>
<div class="container my-5">