<?php
require 'db.php';
$id = $_GET['id'];
$sql = 'SELECT * FROM employé WHERE id=:id';
$statement = $connection->prepare($sql);
$statement->execute([':id' => $id]);
$person = $statement->fetch(PDO::FETCH_OBJ);
if (
    isset($_POST['cin']) &&
    isset($_POST['nom']) &&
    isset($_POST['dateN']) &&
    isset($_POST['dateD']) &&
    isset($_POST['salaire']) &&
    isset($_POST['service'])
) {
    $nom = $_POST['nom'];
    $cin = $_POST['cin'];
    $dateN = $_POST['dateN'];
    $dateD = $_POST['dateD'];
    $salaire = $_POST['salaire'];
    $service = $_POST['service'];
    $sql =
        'UPDATE employé SET cin=:cin, nom=:nom, dateN=:dateN, dateD=:dateD, salaire=:salaire, service=:service WHERE id=:id';
    $statement = $connection->prepare($sql);
    if (
        $statement->execute([
            ':cin' => $cin,
            ':nom' => $nom,
            ':dateN' => $dateN,
            ':dateD' => $dateD,
            ':salaire' => $salaire,
            ':service' => $service,
            ':id' => $id
        ])
    ) {
        $message = 'Donnée mise à jour avec succès';
    }
}
?>


<?php require 'header.php'; ?>
<div class="container">
 <div class="card mt-5">
  <div class="card-header">
   <h2>Mise à jour d'un étudiant</h2>
  </div>
  <div class="card-body">
   <?php if (!empty($message)): ?>
    <div class="alert alert-success">
     <?= $message ?>
    </div>
   <?php endif; ?>
   <form method="post">
   <div class="form-group">
     <label for="cin">CIN</label>
     <input value="<?= $person->cin; ?>" type="text" name="cin" id="cin" class="form-control">
    </div>
    <div class="form-group">
     <label for="nom">Nom complet</label>
     <input value="<?= $person->nom; ?>" type="text" name="nom" id="nom" class="form-control">
    </div>
    <div class="form-group">
     <label for="dateN">Date de naissance</label>
     <input value="<?= $person->dateN; ?>" type="date" name="dateN" id="dateN" class="form-control">
    </div>
    <div class="form-group">
     <label for="dateD">Date d'embauche</label>
     <input value="<?= $person->dateD; ?>" type="date" name="dateD" id="dateD" class="form-control">
    </div>
    <div class="form-group">
     <label for="salaire">Salaire</label>
     <input value="<?= $person->salaire; ?>" type="text" name="salaire" id="salaire" class="form-control">
    </div>
    <div class="form-group">
     <label for="nom">Service</label>
     <select value="<?= $person->service; ?>" name="service" id="service" class="form-control">
       <option>Ressources humaines</option>
       <option>Digitalisation</option>
       <option>Finance</option>
       <option>Production</option>
     </select>
    </div>
    <div class="form-group">
     <button type="submit" class="btn btn-info">Mise à jour d'un étudiant</button>
    </div>
   </form>
  </div>
 </div>
</div>
