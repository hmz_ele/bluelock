<?php
  require 'db.php';
  $message = '';
  if (isset ($_POST['cin']) && isset($_POST['nom']) && isset($_POST['dateN']) && isset($_POST['dateD']) && isset($_POST['salaire']) && isset($_POST['service']) ) {
    $nom = $_POST['nom'];
    $cin = $_POST['cin'];
    $dateN = $_POST['dateN'];
    $dateD = $_POST['dateD'];
    $salaire = $_POST['salaire'];
    $service = $_POST['service'];
    $sql = 'INSERT INTO employé(cin,nom,dateN,dateD,salaire,service) VALUES(:cin,:nom,:dateN,:dateD,:salaire,:service)';
    $statement = $connection->prepare($sql);
    if ($statement->execute([':cin' => $cin, ':nom' => $nom, ':dateN' => $dateN, ':dateD' => $dateD, ':salaire' => $salaire, ':service' => $service])) {
      $message = 'Donnée créée avec succès';
    }
  }
?>


<?php require 'header.php'; ?>
<div class="container">
 <div class="card mt-5">
  <div class="card-header">
   <h2>Créer un employé</h2>
  </div>
  <div class="card-body">
   <?php if(!empty($message)): ?>
    <div class="alert alert-success">
     <?= $message; ?>
    </div>
   <?php endif; ?>
   <form method="post">
   <div class="form-group">
     <label for="cin">CIN</label>
     <input type="text" name="cin" id="cin" class="form-control">
    </div>
    <div class="form-group">
     <label for="nom">Nom complet</label>
     <input type="text" name="nom" id="nom" class="form-control">
    </div>
    <div class="form-group">
     <label for="dateN">Date de naissance</label>
     <input type="date" name="dateN" id="dateN" class="form-control">
    </div>
    <div class="form-group">
     <label for="dateD">Date d'embauche</label>
     <input type="date" name="dateD" id="dateD" class="form-control">
    </div>
    <div class="form-group">
     <label for="salaire">Salaire</label>
     <input type="text" name="salaire" id="salaire" class="form-control">
    </div>
    <div class="form-group">
     <label for="nom">Service</label>
     <select name="service" id="service" class="form-control">
       <option>Ressources humaines</option>
       <option>Digitalisation</option>
       <option>Finance</option>
       <option>Production</option>
     </select>
    </div>
    <div class="form-group">
     <button type="submit" class="btn btn-info">Ajouter un employé</button>
    </div>
   </form>
  </div>
 </div>
</div>
